L4 Head Trim
============

Utility for cutting off everything that's beyond L4 segment. This utility is intended for preparing packets for further analysis.

Usage
-----
This utility needs just specify on which interface it should listen and (possibly output where it should write packet headers).

 - Basic usage:

    .. code-block:: sh

        $ l4headtrim --help

 - With specified interface:

    .. code-block:: sh

        $ l4headtrim --interface eth1

 - With file as output:

    .. code-block:: sh

        $ l4headtrim --interface eth1 --output file:///tmp/capture.headers

 - Add verbosity to logs:

    .. code-block:: sh

        $ l4headtrim -i em1 -o tcp://remote.collector.example.com:12345 -vvv 2> /var/log/l4headtrim.log

   Application uses stderr file descriptor for logging.