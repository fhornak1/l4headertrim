#[macro_use]
extern crate clap;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;

extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate rmp_serde as rmps;

extern crate stderrlog;
extern crate pcap;
extern crate byteorder;
extern crate libc;
extern crate md5;

mod header_cut;
mod writer;
mod payload;
mod utils;

use header_cut::cut_header;
use writer::{OutputWriter, create_writer};
use std::str::FromStr;

#[derive(Debug, Eq, PartialEq)]
pub enum Output {
    StdOut,
    Udp {host: String, port: u16},
    Tcp {host: String, port: u16},
    File {path: String}
}

impl Output {
    pub fn new(output_arg: &str) -> Result<Output, String> {
        let proto_desc = output_arg.splitn(2, "://").collect::<Vec<&str>>();

        match proto_desc[0] {
            "stdout" => Ok(Output::StdOut),
            "file" => Ok(Output::File {path: proto_desc[1].to_owned() }),
            "tcp" => {
                let (host, port) = Output::split_host_port(proto_desc[1]).unwrap();

                Ok(Output::Tcp {host, port})
            },
            "udp" => {
                let (host, port) = Output::split_host_port(proto_desc[1]).unwrap();

                Ok(Output::Udp {host, port})
            },
            _ => Err(format!("Unknown output type: {}", proto_desc[0]))
        }
    }

    fn split_host_port(host_port_str: &str) -> Result<(String, u16), &str> {
        let host_port_vec = host_port_str.splitn(2, ":").collect::<Vec<&str>>();

        if host_port_vec.len() < 2 {
            return Err("Host and port should be specified");
        }

        let port: u16 = host_port_vec[1].parse().unwrap();

        Ok((host_port_vec[0].to_owned(), port))
    }
}

#[derive(Debug)]
pub struct Config {
    output: Output,
    interface: String,
    print_ifaces: bool,
    batch_size: usize,
    verbosity: usize,
    quiet: bool,
    timestamp: stderrlog::Timestamp,
}

impl Config {

    pub fn get_output(&self) -> &Output {
        &self.output
    }

    pub fn get_interface(&self) -> &str {
        &self.interface
    }

    pub fn print_interfaces(&self) -> bool {
        self.print_ifaces
    }

    pub fn get_batch_size(&self) -> usize {
        self.batch_size
    }

    pub fn load() -> Config {
        let matches = clap_app!(l4headtrim =>
            (version: "0.1.1")
            (author: "Filip Hornak <fhornak1@gmail.com>")
            (about: "Application processing pcap stream and cutting off payload from packet headers")
            (@arg IFACE: -i --interface +takes_value default_value[lo] "Sets interface on which will listen")
            (@arg OUTPUT: -o --output +takes_value default_value[stdout] "Sets output file descriptor (handle) [possible values: `stdout`, `udp://<HOST>[:PORT]`, `tcp://<HOST>[:PORT]`, `file://<PATH>`]")
            (@arg BATCH_SIZE: -b --("batch-size") +takes_value "Sets how many headers will be stored in single batch [default: 5]")
            (@arg PRINT_IFACES: -l --("list-interfaces") "Prints available interfaces!")
            (@arg VERBOSITY: -v..."Sets the verbosity level")
            (@arg TIMESTAMP: -t +takes_value default_value[us] possible_value[none sec ms us ns] "Prepends log lines with timestamp. Defaults to Microsecond")
            (@arg QUIET: -q --quiet "Silence all output")
        ).get_matches();

        let output_arg = matches.value_of("OUTPUT").unwrap_or("stdout");
        let batch_size_arg = matches.value_of("BATCH_SIZE").unwrap_or("5");
        let output = Output::new(output_arg).unwrap();
        let print_ifaces = match matches.occurrences_of("PRINT_IFACES") {
            0 => false,
            _ => true
        };
        let ts = matches.value_of("TIMESTAMP").map(|v| {
            stderrlog::Timestamp::from_str(v).unwrap_or_else(|_| {
                clap::Error {
                    message: "invalid value for 'timestamp'".into(),
                    kind: clap::ErrorKind::InvalidValue,
                    info: None,
                }.exit()
            })
        }).unwrap_or(stderrlog::Timestamp::Off);

        Config {
            output,
            print_ifaces,
            interface: matches.value_of("IFACE").unwrap_or("lo").to_owned(),
            batch_size: batch_size_arg.parse().unwrap(),
            quiet: matches.is_present("QUIET"),
            verbosity: matches.occurrences_of("VERBOSITY") as usize,
            timestamp: ts,
        }
    }

    pub fn format(&self) -> String {
        format!("{:?}", self)
    }
}

lazy_static! {
    static ref CONFIG: Config = Config::load();
}

fn convert_to_microseconds(timeval: libc::timeval) -> u64 {
    let secs = timeval.tv_sec as u64;

    let nanos: u64 = secs * 1_000_000 + timeval.tv_usec as u64;

    nanos
}

//output format:
// 0x00 0x00 0x01 <packet_size> 0x00 0x00 0x02 <timestamp> 0x00 0x00 0x03 <packet_octets>
// or
// <packet_size 2 octets u16 big endian> <timestamp 8 octets u64 big endian> <packet n octets defined in size>
fn main() {
    stderrlog::new()
        .module(module_path!())
        .quiet(CONFIG.quiet)
        .verbosity(CONFIG.verbosity)
        .timestamp(CONFIG.timestamp)
        .init()
        .unwrap();
    info!("Starting {} with following configuration: {}", crate_name!(), CONFIG.format());
    if CONFIG.print_interfaces() {
        for dev in pcap::Device::list().unwrap() {
            info!("Found device: {}", dev.name);
            println!("Found device: {}", dev.name);
            if let Some(desc) = dev.desc {
                info!("Device description: {}", desc);
                println!("Device description: {}", desc);
            }
        }
    } else {
        let cap_device = match pcap::Device::list().unwrap().into_iter().filter(|dev| {dev.name == CONFIG.get_interface()}).next() {
            Some(dev) => {
                info!("Found device [{}] in list of devices. Description: {}", dev.name.clone(), dev.desc.clone().unwrap_or("None".to_owned()));
                dev
            }
            None => {
                error!("Device [{}] not found!", CONFIG.get_interface());
                panic!("Device [{}] not found!", CONFIG.get_interface());
            }
        };

        let mut capture = match pcap::Capture::from_device(cap_device).unwrap().promisc(false).snaplen(5000).open() {
            Ok(cap) => {
                info!("Capture for device [{}] successfully open", CONFIG.get_interface());
                cap
            },
            Err(e) => {
                error!("Opening of capture for device [{}] failed!", CONFIG.get_interface());
                panic!("Opening of capture for device [{}] failed!", CONFIG.get_interface());
            }
        };

        let mut writer = create_writer(CONFIG.get_output(), CONFIG.get_batch_size()).unwrap();

        while let Ok(pkt) = capture.next() {
            match cut_header(pkt.data) {
                Ok(header) => {
                    match writer.write_record(convert_to_microseconds(pkt.header.ts), header) {
                        Err(e) => {
                            error!("Following occurred while writing record to output writer: {}", e);
                            panic!("Following occurred while writing record to output writer: {}", e);
                        },
                        _ => {}
                    }
                },
                Err(e) => warn!("{}", e)
            }
        }
    }
}
