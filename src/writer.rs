use std::rc::Rc;
use std::fs::{OpenOptions, File};
use std::net::{TcpStream, UdpSocket};
use std::io::{Write, Error};
use payload::{BatchRecord, HeaderRecord};
use rmps::{Serializer, to_vec_named, to_vec};

pub trait OutputWriter {
    fn write_record(&mut self, timestamp: u64, header: &[u8]) -> Result<(), String>;
}

pub struct BatchWriter<W: Write + Sized> {
    capacity: usize,
    buffer: Vec<HeaderRecord>,
    writer: W
}

impl <W> BatchWriter<W>
    where W: Write + Sized
{
    pub fn new(capacity: usize, writer: W) -> BatchWriter<W> {
        BatchWriter {
            capacity,
            buffer: Vec::new(),
            writer
        }
    }
}

impl <W>OutputWriter for BatchWriter<W>
    where W: Write + Sized
{
    fn write_record(&mut self, timestamp: u64, header: &[u8]) -> Result<(), String> {
        {
            let header = HeaderRecord::new(timestamp, header);
            self.buffer.push(header);
        }
        if self.buffer.len() >= self.capacity {
            {
                let batch = BatchRecord::new(&self.buffer);
                self.writer.write(&to_vec_named(&batch).unwrap()[..]).unwrap();
                self.writer.flush().unwrap();
            }
            self.buffer.clear();
        };

        Ok(())
    }
}

pub struct FileWriter {
    file: File,
    path: String
}

impl FileWriter {
    pub fn new(path: String) -> FileWriter {
        let file = OpenOptions::new()
            .write(true)
            .append(true)
            .read(false)
            .create(true)
            .open(path.clone())
            .unwrap();
        FileWriter {
            file,
            path
        }
    }
}

impl Write for FileWriter {
    fn write(&mut self, buf: &[u8]) -> Result<usize, Error> {
        self.file.write(buf)
    }

    fn flush(&mut self) -> Result<(), Error> {
        self.file.flush()
    }
}

impl OutputWriter for FileWriter {
    fn write_record(&mut self, timestamp: u64, header: &[u8]) -> Result<(), String> {
        match self.file.write_all(&to_vec_named(&HeaderRecord::new(timestamp, header)).unwrap()) {
            Ok(()) => Ok(()),
            Err(e) => Err(format!("Writing header to file `{}` failed, due to {}", self.path, e))
        }
    }
}

pub struct TcpWriter {
    stream: TcpStream
}

impl Write for TcpWriter {
    fn write(&mut self, buf: &[u8]) -> Result<usize, Error> {
        self.stream.write(buf)
    }

    fn flush(&mut self) -> Result<(), Error> {
        self.stream.flush()
    }
}

impl OutputWriter for TcpWriter {
    fn write_record(&mut self, timestamp: u64, header: &[u8]) -> Result<(), String> {
        match self.stream.write_all(&to_vec_named(&HeaderRecord::new(timestamp, header)).unwrap()) {
            Ok(()) => Ok(()),
            Err(e) => Err(format!("Writing to Tcp stream `{}->{}` failed, due to {}",
                                  self._local_address(), self._peer_address(), e))
        }
    }
}

impl TcpWriter {
    pub fn new(bind_address: String) -> TcpWriter {
        let stream = TcpStream::connect(bind_address).unwrap();
        TcpWriter {
            stream
        }
    }

    fn _local_address(&self) -> String {
        TcpWriter::_socket_address(self.stream.local_addr())
    }

    fn _peer_address(&self) -> String {
        TcpWriter::_socket_address(self.stream.peer_addr())
    }

    fn _socket_address(address_res: ::std::io::Result<::std::net::SocketAddr>) -> String {
        match address_res {
            Ok(address) => {
                address.to_string()
            },
            Err(e) => {
                "Unknown".to_owned()
            }
        }
    }
}

pub fn create_writer(output: &::Output, batch_size: usize) -> Result<Box<OutputWriter>, String> {
    match output {
        ::Output::File {path} => {
            Ok(_wrap_in_batch_writer(FileWriter::new(path.clone()), batch_size))
        },
        ::Output::Tcp {host, port} => {
            Ok(_wrap_in_batch_writer(TcpWriter::new(format!("{}:{}", host, port)), batch_size))
        },
        _ => {
            Err(format!("Unrecognized output writer `{:?}`", output))
        }
    }
}

fn _wrap_in_batch_writer<W: Write + OutputWriter + 'static>(writer: W, batch_size: usize) -> Box<OutputWriter> {
    match batch_size {
        0 => {
            Box::new(writer)
        },
        size => {
            Box::new(BatchWriter::new(batch_size, writer))
        }
    }
}

#[cfg(test)]
mod tests {
}
