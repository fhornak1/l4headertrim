use std::fmt::Display;

pub fn consume_result<O, E>(res: Result<O, E>, success: &'static str, error: &'static str)
    -> O
    where E: Display
{
    match res {
        Ok(o) => {
            info!("{}", success);
            o
        },
        Err(e) => {
            error!("{} {}", error, e);
            panic!("{} {}", error, e);
        }
    }
}

pub fn err_to_owned<O>(res: Result<O, &str>) -> Result<O, String> {
    match res {
        Ok(o) => Ok(o),
        Err(msg) => Err(msg.to_owned())
    }
}