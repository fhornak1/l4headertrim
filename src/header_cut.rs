use utils::err_to_owned;


pub mod ethernet_type {
    pub const IPV4: u16 = 0x0800;
    pub const IPV6: u16 = 0x86DD;
    pub const ARP: u16 =  0x0806;
    pub const VLAN: u16 = 0x8100;
}


pub mod ipv6_next_header {

    pub const HOP_BY_HOP: u8 = 0;
    pub const DESTINATION_OPTS: u8 = 60;
    pub const ROUTING: u8 = 43;
    pub const FRAG: u8 = 44;
    pub const AH: u8 = 51;
    pub const ESP: u8 = 50;
    pub const MOBILITY: u8 = 135;
    pub const HOST_IDENTITY: u8 = 139;
    pub const SHIM6: u8 = 140;
    pub const RESERVED1: u8 = 253;
    pub const RESERVED2: u8 = 254;

    pub const NEXT_HEADERS: [u8; 11] = [
        HOP_BY_HOP,
        DESTINATION_OPTS,
        ROUTING,
        FRAG,
        AH,
        ESP,
        MOBILITY,
        HOST_IDENTITY,
        SHIM6,
        RESERVED1,
        RESERVED2
    ];
}

pub mod ip_protocol {
    pub const ICMP: u8 = 0x01;
    pub const IGMP: u8 = 0x02;
    pub const TCP: u8 = 0x06;
    pub const EGP: u8 = 0x08;
    pub const UDP: u8 = 0x11;
    pub const UDP_LITE: u8 = 0x88;
    pub const UNKNOWN: u8 = 0xff;

    pub const IP_PROTOCOLS: [u8; 3] = [
        ICMP,
        TCP,
        UDP
    ];
}

fn get_frame_header_length(frame: &[u8]) -> Result<usize, &'static str> {
    if frame.len() < 14 {
        Err("Frame is too short, required length is at least 14 octets")
    } else if _has_vlan_tag(frame) {
        Ok(18)
    } else {
        Ok(14)
    }
}

fn _has_vlan_tag(frame: &[u8]) -> bool {
    _to_u16_big_endian(frame, 12).unwrap() == ethernet_type::VLAN
}

fn _to_u16_big_endian(octets: &[u8], from: usize) -> Result<u16, &'static str> {
    if octets.len() < from + 1 {
        Err("Array reference has not sufficient size")
    } else {
        Ok((octets[from] as u16) << 8 | octets[from + 1] as u16)
    }
}

fn extract_ether_type(frame: &[u8]) -> Result<u16, &'static str> {
    match _has_vlan_tag(frame) {
        true => _to_u16_big_endian(frame, 16),
        false => _to_u16_big_endian(frame, 12)
    }
}

fn get_ipv4_header_length(packet: &[u8]) -> Result<usize, &'static str> {
    if packet.len() < 1 {
        return Err("IPv4 packet length is less then one byte!");
    }
    let ihl = packet[0] & 0x0f;

    Ok(ihl as usize * 4)
}

fn get_ipv4_protocol(packet: &[u8]) -> Result<u8, &'static str> {
    if packet.len() < 9 {
        return Err("IPv4 packet length is less then 9 bytes. Can not read IP protocol");
    }
    Ok(packet[9])
}

fn get_ipv6_header_length_and_next_header(packet: &[u8]) -> Result<(usize, u8), String> {
    if packet.len() < 40 {
        return Err(format!("IPv4 packet length [len={}] is less then 40 octets!", packet.len()));
    }

    let mut next_header = packet[7].clone();
    let mut header_size: usize = 40;
    let mut cursor_pos: usize = 0;

    while ipv6_next_header::NEXT_HEADERS.contains(&next_header) {
        cursor_pos += header_size;
        if cursor_pos >= packet.len() {
            return Err(format!("IPv6 NextHeader cursor position [position={}] exceeded upper limit [packet length={}]", cursor_pos, packet.len()))
        }
        header_size = _get_header_size(&packet[cursor_pos .. ]);
        next_header = packet[cursor_pos];
    }

    Ok((cursor_pos + header_size, next_header))
}

fn _get_header_size(slice: &[u8]) -> usize {
    slice[1] as usize + 8
}

fn get_arp_header_length(frame: &[u8]) -> Result<usize, &'static str> {
    Ok(28)
}

fn get_tcp_header_length(frame: &[u8]) -> Result<usize, &'static str> {
    if frame.len() < 12 {
        return Err("TCP segment length is less than 12. Can not read data offset.");
    }
    Ok( (((frame[12] & 0xF0) >> 4) as usize) * 4 )
}

fn calculate_packet_header_length(packet: &[u8]) -> Result<usize, String> {
    let mut len = get_frame_header_length(packet)?;

    let proto = match err_to_owned(extract_ether_type(packet))? {
        ethernet_type::ARP => {
            len += 28;
            Ok(0)
        },
        ethernet_type::IPV4 => {
            let ip_proto = err_to_owned(get_ipv4_protocol(&packet[len..]))?;
            len += err_to_owned(get_ipv4_header_length(&packet[len..]))?;
            Ok(ip_proto)
        },
        ethernet_type::IPV6 => {
            let (ipv6len, ip_proto) = get_ipv6_header_length_and_next_header(&packet[len..])?;
            len += ipv6len;
            Ok(ip_proto)
        }
        eth_type => Err(format!("Unable to calculate packet length, due to unknown ethernet type [ether_type={}].", eth_type))
    }?;

    match proto {
        0 => Ok(len),
        ip_protocol::ICMP => Ok(len + 8),
        ip_protocol::TCP => {
            let tcp_len = get_tcp_header_length(&packet[len..])?;
            Ok(len + tcp_len)
        },
        ip_protocol::UDP => Ok(len + 8),
        unknown => Err(format!("Unable to calculate segment length, due to unknown protocol [protocol={}].", unknown))
    }
}

pub fn cut_header(packet: &[u8]) -> Result<&[u8], String> {
    let header_len = calculate_packet_header_length(packet)?;
    if packet.len() < header_len {
        Err(format!("Can not create slice of packet header, packet is too short! Packet len: {} slice len: {}", packet.len(), header_len))
    } else {
        Ok(&packet[..header_len])
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    const ETHER_FRAME_WO_VLAN: [u8; 15] = [
        0x11, 0x11, 0x11, 0x11, 0x11, 0x11, // dst mac
        0x22, 0x22, 0x22, 0x22, 0x22, 0x22, // src mac
        0x08, 0x00, // ether type
        0x45, // version and ihl
    ];

    const ETHER_FRAME_W_VLAN: [u8; 19] = [
        0x11, 0x11, 0x11, 0x11, 0x11, 0x11, // dst mac
        0x22, 0x22, 0x22, 0x22, 0x22, 0x22, // src mac
        0x81, 0x00, 0xff, 0x11, // tag
        0x08, 0x00, // ether type
        0x45, // version and ihl
    ];


    #[test]
    fn test_get_frame_header_length() {
        let len = get_frame_header_length(&ETHER_FRAME_WO_VLAN[..]).unwrap();
        assert_eq!(14, len);
    }

    #[test]
    fn test_get_frame_header_length_vlan() {
        assert_eq!(18, get_frame_header_length(&ETHER_FRAME_W_VLAN[..]).unwrap())
    }

    #[test]
    fn test_extract_ether_type() {
        assert_eq!(0x0800u16, extract_ether_type(&ETHER_FRAME_W_VLAN[..]).unwrap());
        assert_eq!(0x0800u16, extract_ether_type(&ETHER_FRAME_WO_VLAN[..]).unwrap());
    }

    const IPV4_PACKET: [u8; 24] = [
        0x45, // version ihl
        0x00, // dscp ecn
        0x01, 0x00, // total packet length
        0x00, 0x00, // identification
        0x00, 0x00, // flags + fragment offset
        0x40, 0x06, // ttl + protocol
        0x00, 0x00, // header chksum
        0x0A, 0x0A, 0x0A, 0x0A, // src address
        0x14, 0x14, 0x14, 0x14, // dst address
        0x10, 0x00, 0x10, 0x01 // tcp src/dst ports
    ];

    #[test]
    fn test_get_ipv4_length() {
        assert_eq!(20, get_ipv4_header_length(&IPV4_PACKET[..]).unwrap());
        let pkt_w_options: [u8; 24] = [
            0x46, // version ihl
            0x00, // dscp ecn
            0x01, 0x00, // total packet length
            0x00, 0x00, // identification
            0x00, 0x00, // flags + fragment offset
            0x40, 0x06, // ttl + protocol
            0x00, 0x00, // header chksum
            0x0A, 0x0A, 0x0A, 0x0A, // src address
            0x14, 0x14, 0x14, 0x14, // dst address
            0x10, 0x00, 0x10, 0x01 // options
        ];

        assert_eq!(24, get_ipv4_header_length(&pkt_w_options[..]).unwrap());
    }

    #[test]
    fn test_get_l4_protocol() {
        assert_eq!(ip_protocol::TCP, get_ipv4_protocol(&IPV4_PACKET[..]).unwrap());
    }

    fn test_get_len_ipv6() {

    }
}