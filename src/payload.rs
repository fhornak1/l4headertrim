use std::rc::Rc;
use std::io::{self, Write};
use byteorder::{BigEndian, WriteBytesExt, ByteOrder};
use md5::{compute, Digest};
use std::ops::Range;
use serde::Serialize;
use rmps::Serializer;
use rmps::encode;


#[derive(Debug, PartialEq, Serialize)]
pub struct HeaderRecord {
    timestamp: u64,
    payload: Vec<u8>
}

impl HeaderRecord {
    pub fn new(timestamp: u64, payload: &[u8]) -> HeaderRecord {
        HeaderRecord {
            timestamp,
            payload: payload.iter().map(|o| o.clone()).collect()
        }
    }
}

#[derive(Debug, PartialEq, Serialize)]
pub struct BatchRecord<'a> {
    chksum: [u8; 16],
    headers: &'a Vec<HeaderRecord>,
}

impl <'a> BatchRecord <'a> {
    pub fn new(headers: &'a Vec<HeaderRecord>) -> BatchRecord {
        let header_vec = encode::to_vec_named(headers).unwrap();
        let chksum = compute(header_vec);

        BatchRecord {
            chksum: chksum.0,
            headers
        }
    }
}
